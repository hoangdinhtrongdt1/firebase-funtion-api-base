import { Server, Request, Response } from "restify";
import { HTTPS_CODE } from "../constant";
import AuthService from "../services/auth-service";
import BaseController from "./base-controller";

class AuthController implements BaseController {
    init(app: Server): void {
        app.post('/auth/sign-up', this.signUp);
        app.post('/auth/sign-in', this.signIn);
        app.post('/auth/forgot-password', this.forgotPassword);
    }

    signUp = async (req: Request, res: Response) => {
       const authService = new AuthService();
       const isUserExit = await authService.checkUserAlreadyExist(req.body)
       if (isUserExit) {
           return res.send(HTTPS_CODE.CONFLICT, 'User Already Exist')
       }
       const user = await authService.createUser(req.body);
       return res.send(user)
    }

    signIn = async (req: Request, res: Response) => {
       const authService = new AuthService();
       const user = await authService.checkMapchingUserNamePassword(req.body)
       if (user === HTTPS_CODE.BAD_REQUEST) {
            return res.send(HTTPS_CODE.BAD_REQUEST, 'wrong username or password')
       }
       if (user === HTTPS_CODE.NOT_FOUND) {
            return res.send(HTTPS_CODE.NOT_FOUND, 'user not exist')
       }
       return res.send(user)
    }

    forgotPassword = async (req: Request, res: Response) => {
        const authService = new AuthService();
        await authService.forgotPassword(req.body.email)
        res.send('Message sent successfully')
    }    
}

export default AuthController; 