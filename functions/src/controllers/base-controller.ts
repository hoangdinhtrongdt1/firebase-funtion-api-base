import { Server } from "restify";

export default interface BaseController {
    init(app: Server): void;
}