import { Server, Request, Response } from "restify";
import BaseController from "./base-controller";

class UserController implements BaseController {
    init(app: Server): void {
        app.get('/user/info', this.getInfoUser)
    }

    getInfoUser = (req: Request, res: Response) => {
        res.send('getInfoUser');
    }
}
export default UserController;