import * as jwt from 'jsonwebtoken';
import { Config } from '../config';

export interface Itoken {
    phone: string;
    iat: number;
    exp: number;
    password: string;
}

export default class Authentication {
    static isToken(token: string) {
        return /Bearer\s[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/.test(token);
    }

    static generateToken(phone: string): string {
        const token = jwt.sign({phone}, Config.JWT_SECRET || "", {
            algorithm: "HS512",
            expiresIn: "365d",
        });
        return `Bearer ${token}`;
    }

    static generatePass(password: string): string {
        const pass = jwt.sign({password}, Config.JWT_SECRET || "", {
            algorithm: "HS512",
            expiresIn: "365d",
        });
        return pass;
    }

    static verifyToken(token: string): boolean {
        const data: Itoken = jwt.verify(
            token,
            Config.JWT_SECRET || "",
            {algorithms: ["HS512"]},
        ) as Itoken;

        if (data.iat * 1000 - new Date().getTime() > 0) return false;
        if (data.exp * 1000 - new Date().getTime() <= 0) return false;
        return true;
    }

    static refreshToken(token: string): string {
        const data: Itoken = jwt.verify(
            token,
            Config.JWT_SECRET || "",
            {algorithms: ["HS512"]},
        ) as Itoken;
        if (data.exp - new Date().getTime() / 1000 < 60 * 60) {
            return Authentication.generateToken(data.phone);
        }
        return token;
    }

    static getUserIdByToken(token: string): Pick<Itoken, "phone"> {
        return jwt.verify(token, Config.JWT_SECRET || "", {
            algorithms: ["HS512"],
        }) as Pick<Itoken, "phone">;
    }

    static getPasswordByToken(token: string): Pick<Itoken, "password"> {
        return jwt.verify(token, Config.JWT_SECRET || "", {
            algorithms: ["HS512"],
        }) as Pick<Itoken, "password">;
    }

    
}