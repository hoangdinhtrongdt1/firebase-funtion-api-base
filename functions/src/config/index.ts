export const Config = {
  name: 'TEST FIREBASE FUNTION',
  version: '1.0.0',
  regionCountry: 'asia-east2',
  runtimeOpts: {
    timeoutSeconds: 100,
    memory: '1GB'
  },
  JWT_SECRET: 'TEST-FIREBASE-FUNTION-API'
}