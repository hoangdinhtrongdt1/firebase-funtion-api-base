export const COLLECTION = {
    USER: 'users',
    TOKEN: 'tokens'
}

export const DOCUMENTS = {
    PHONE: 'phone',
    EMAIL: 'email',
    UID: 'uid'
}

export const HTTPS_CODE = {
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    CONFLICT: 409,
    OK: 200,
    CREATED: 201
}