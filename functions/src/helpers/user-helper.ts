import { UserModel } from "../model/user-model";

class UserHelper {
    static checkEmptyUser(snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>, username?: string): UserModel | null {
        let user = null;
        if (snapshot.empty) {
            if (username) {
                console.log(`No documents matching ${username}`);
            }
            return null;
        }
        snapshot.forEach(doc => {
            user = doc.data();
        });
        return user
    }
    static makeid = (length: number): string => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }
}

export default UserHelper