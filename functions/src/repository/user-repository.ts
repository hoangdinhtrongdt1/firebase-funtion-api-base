
import { Token, UserModel } from "../model/user-model";
import { COLLECTION, DOCUMENTS } from "../constant";
import * as admin from 'firebase-admin';
import UserHelper from "../helpers/user-helper";

class UserRepository {
    fireStore = admin.firestore(); 
    createUser = async (userModel: UserModel) : Promise<UserModel> => {
        const userCollection = await this.fireStore.collection(COLLECTION.USER).add(userModel)
        userModel.uid = userCollection.id
        await userCollection.update(userModel)
        return userModel;
    }

    createToken = async (token: Token): Promise<Token> => {
        await this.fireStore.collection(COLLECTION.TOKEN).add(token)
        return token
    }

    getUserByPhone = async (phone: string): Promise<UserModel | null> => {
        const userCollection = await this.fireStore.collection(COLLECTION.USER);
        const snapshot = await userCollection.where(DOCUMENTS.PHONE, '==', phone).get()
        return UserHelper.checkEmptyUser(snapshot, phone)
    }

    getUserByPhoneOrMail = async (userName: string): Promise<UserModel | null> => {
        const userCollection = await this.fireStore.collection(COLLECTION.USER);
        const snapshot = await userCollection.where(DOCUMENTS.PHONE, '==', userName).get()
        const user =  UserHelper.checkEmptyUser(snapshot)
        if (user) {
            return user
        }
        return await this.getUserByMail(userName)

    }

    getUserByMail = async (userName: string): Promise<UserModel | null> => {
        const userCollection = await this.fireStore.collection(COLLECTION.USER);
        const snapshot = await userCollection.where(DOCUMENTS.EMAIL, '==', userName).get()
        return UserHelper.checkEmptyUser(snapshot, userName)
    }

    
    getUserById= async (uid: string): Promise<UserModel | null> => {
        const userCollection = await this.fireStore.collection(COLLECTION.USER);
        const snapshot = await userCollection.where(DOCUMENTS.UID, '==', uid).get()
        return UserHelper.checkEmptyUser(snapshot, uid)
    }

    
}

export default UserRepository