export interface UserModel {
    uid: string;
    phone: string;
    name: string;
    address: string;
    email: string;
    password: string;
    createDate: string;
    updateDate: string;
}

export interface UserRespont {
    uid: string;
    phone: string;
    name: string;
    address: string;
    email: string;
    token: string;
}

export interface UserRequest {
    phone: string;
    name: string;
    address: string;
    email: string;
    password: string;
}

export interface Token {
    uid: string;
    jwt: string;
    createDate: string;
    updateDate: string;
}

export interface SignIn {
    userName: string;
    password: string;
}