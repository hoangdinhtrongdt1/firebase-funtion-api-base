import * as functions from "firebase-functions";
import * as restify from 'restify';
import { Config } from "./config";
import { controlers } from "./controllers";
import * as admin from 'firebase-admin';
// firestore
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
});
//resfity API
const app = restify.createServer();
app.use(restify.plugins.queryParser());
//log request APi
app.get('/', (req, res) => res.send(`name: ${Config.name}, version: ${Config.version}`));
app.use((request, response, next) => {
    console.log("API Request: " + JSON.stringify({
        method: request.method,
        url: request.url,
        params: request.params,
        body: request.body
    }));
    console.log("API Response: " + JSON.stringify({
        method: request.method,
        url: request.url,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage
    }));
    next();
});
// run 
controlers.forEach(controler => controler.init(app));

// @ts-ignore: Unreachable code error
export const api = functions.runWith(Config.runtimeOpts).region(Config.regionCountry).https.onRequest(app._onRequest.bind(app));