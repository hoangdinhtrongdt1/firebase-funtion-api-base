import * as moment from 'moment';
import Authentication from "../auth";
import { HTTPS_CODE } from "../constant";
import { SignIn, Token, UserModel, UserRequest, UserRespont } from "../model/user-model";
import UserRepository from "../repository/user-repository";
import * as nodemailer from 'nodemailer';
import UserHelper from '../helpers/user-helper';

class AuthService {
    userRepository = new UserRepository();
    
    createUser = async (userRequest: UserRequest) : Promise<UserRespont> => {
        const date = moment().format()
        const password = Authentication.generatePass(userRequest.password)
        const userParams: UserModel = {
            uid: '',
            ...userRequest,
            password,
            createDate: date,
            updateDate: date
        }
        const user = await this.userRepository.createUser(userParams)
        const token = await this.createToken(userRequest.phone, user.uid)
       return this.conventRespont(user, token.jwt)
    }

    checkUserAlreadyExist = async (userRequest: UserRequest) : Promise<boolean> => {
        const user =  await this.userRepository.getUserByPhone(userRequest.phone);
        if (user) {
            return true
        }
        return false
    }

    checkMapchingUserNamePassword = async (signinRequest: SignIn): Promise<UserRespont | number> => {
        const user =  await this.userRepository.getUserByPhoneOrMail(signinRequest.userName);
        if (user) {
            const passToken = await Authentication.getPasswordByToken(user.password)
            const isMapinguserName = user.phone === signinRequest.userName || user.email === signinRequest.userName
            if(isMapinguserName && passToken.password === signinRequest.password) {
                const token = await this.createToken(user.phone, user.uid)
                return this.conventRespont(user, token.jwt)
            }
            return HTTPS_CODE.BAD_REQUEST
        }
        return HTTPS_CODE.NOT_FOUND;
    }

    createToken = async (phone: string, uid: string): Promise<Token> => {
        const jwt = Authentication.generateToken(phone)
        const date = moment().format()
        const tokenParams: Token = {
            uid,
            jwt,
            createDate: date,
            updateDate: date
        }
        return await this.userRepository.createToken(tokenParams)
    }

    conventRespont = (user: UserModel, token: string): UserRespont => {
        const userRespont: UserRespont = {
            uid: user.uid,
            name: user.name,
            address: user.address,
            phone: user.phone,
            email: user.email,
            token
        }
        return userRespont
    }

    forgotPassword = async (email: string): Promise<number> => {
        const user = await this.userRepository.getUserByMail(email);
        if (user == null) {
            return HTTPS_CODE.NOT_FOUND
        }
        const newPassword = UserHelper.makeid(8)
         // create reusable transporter object using the default SMTP transport
         const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'appstorecenterconnect@gmail.com',
              pass: 'gicungduoc' // naturally, replace both with your real credentials or an application-specific password
            }
          });
        const info = await transporter.sendMail({
            from: 'appstorecenterconnect@gmail.com', // sender address
            to: email, // list of receivers
            subject: "Forgot Password", // Subject line
            text: `your new password is: ${newPassword}`, // plain text body
        });
        console.log('Email sent: ' + info.response);
        return HTTPS_CODE.OK
    }
}
export default AuthService;